<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract Class Speak {    
    public abstract function say();    
}


Class Tiger extends Speak {
    
    public function say() {
        echo "I am Tiger";
    }
}

Class Cat extends Speak {
    
    public function say() {
        echo "I am Cat";
    }
}

Class Lion extends Speak {
    
    public function say() {
        echo "I am Lion";
    }
}

Class Goat extends Speak {
    
    public function say() {
        echo "I am Goat";
    }
}

$goat = new Tiger();
$cat = new Cat;
$lion = new Lion;
$tiger = new Goat;

$objArray = [$goat, $cat, $lion, $tiger];

shuffle($objArray);

foreach($objArray as $obj)
{
    echo $obj->say();
}
