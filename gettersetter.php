<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Football {
    
    var $country = "";
    var $startedIn = "";
    var $totalTouraments = "";
    var $totalMatches = "";
    var $tier = "";
    
    public function __construct($country, $startedIn, $totalTouraments,$totalMatches,$tier ) {
       $this->country = $country;
       $this->startedIn = $startedIn;
       $this->totalTouraments = $totalTouraments;
       $this->totalMatches = $totalMatches;
       $this->tier = $tier;
    }
    
    public function getCountry()
    {
        return $this->country;
    }
    
    public function setCountry($name) 
    {
        $this->country = $name;
    }
    
    public function getStartedIn()
    {
        return $this->startedIn;
    }
    
    public function setStartedIn($name) 
    {
        $this->startedIn = $name;
    }
    
}

$laLega = new Football("Spain", "1910", "3","38","5");

// don't call directly
//echo $laLega->country;

echo $laLega->getCountry();

// dont set directly
//$laLega->country = "Bangladesh";

$laLega->setCountry("Bangladesh");