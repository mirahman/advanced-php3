<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Account {
    
    private $interestRate = .125;
    public  $totalInvestMent = 0;
    public $year = 0;
    
    
    function __construct($investment , $duration) {
        $this->totalInvestMent = $investment;
        $this->year = $duration;
    }
    
    function getTotalAmount() {
        return sprintf("%.2f", ((1 + $this->interestRate)** $this->year) * $this->totalInvestMent );
    }
    
    function showMeInterestRate() {
        echo ($this->interestRate*100)."%";
    }
    
}

$fdr = new Account(1000, 12);

echo $fdr->showMeInterestRate();

echo "<br />".$fdr->getTotalAmount();