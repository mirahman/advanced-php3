<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Human {
    
    var $name = "Manush";
    var $age = "0";
    
    function showName()
    {
        echo "I am showing name ".$this->name;
    }
    
    function anotherFunction()
    {
        $this->showName();
    }
}

class Asian {
    var $name = "Asian";
    
    function showName()
    {
        echo "I am from Asia";
    }
}

$mizan = new Human;
$rana = new Human();

//echo $mizan->name."<br />";
//echo $rana->name."<br />";

//echo $mizan->showName()."<br />";
//echo $rana->showName()."<br />";

$mizan->name = "Mizan";
$rana->name = "Salahuddin Rana";

echo $mizan->anotherFunction()."<br />";
echo $rana->anotherFunction()."<br />";