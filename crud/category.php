<?php
include "pdo.php";

$sql = "select * from category order by parentID asc";
$query = $dbh->query($sql, PDO::FETCH_OBJ);

$category = [];

foreach($query as $obj)
{
    $category[$obj->parentID][] = $obj;
}

//print_r($category);

function generateTree($id = 0) {
    global $category;
    
    if(!isset($category[$id]) || count($category[$id]) ==0) return "";
    
    $str = "<ul class='cat'>";
    foreach($category[$id] as $obj)
    {
         $str .= "<li><a>".$obj->categoryName."</a>";
         $str .= generateTree($obj->id);
         $str.= "</li>";
    }
    
    $str .= "</ul>";
    
    return $str;
}

include ("header.php");
?>

<h1>Category Tree</h1>

<?php
echo generateTree(0);
?>



<script>

$(function(){
    $("#driver").on('change',function(){
         console.log($(this).val());
         
         $.ajax({
           type: 'get',
            url: 'getdata.php?id='+$(this).val(),
            success: function(text) {
                console.log(text);
                $("#taxi").html(text);
            }
         });


    });
});

</script>
<?php
include ("footer.php");
?>


