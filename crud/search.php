<?php
include "pdo.php";
include ("header.php");

if($_GET) {
$search = $_GET['search'];
$sql = "select * from drivers "
        . "left join taxis on taxis.driverID = drivers.id "
        . "where driverName like '%$search%' or licenseNumber like '%$search%' or dob like '%$search%' or  gender like '%$search%'";
    
$query = $dbh->query($sql);

}
?>

        

            <h1>Search Drivers</h1>
            <br />
            <span class='col-md-6'>

            </span>
            <span class='col-md-6'>
                <form method='get' action='search.php'>
                    <input type='text' name='search' /> <input type='submit' class='btn btn-success' value='Search'>
                    
                </form>
            </span>
            <Br />
            <br />
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Driver Name
                        </th>
                        <th>License #</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Verified?</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                        if($query)
                        {   $i = 1;
                            foreach($query as $row):
                             ?>
                    <tr>
                        <td><?php echo $i++; ?>
                        </td>
                        <td><?php echo $row['driverName']; ?>
                        </td>
                        <td><?php echo $row['licenseNumber']; ?>
                        </td>
                        <td><?php echo $row['dob']; ?>
                        </td>
                        <td><?php echo $row['gender']; ?>
                        </td>
                        <td><?php echo $row['verified']; ?>
                        </td>
                        <td>
                            <a href='update.php?id=<?php echo $row['id']; ?>' class='btn btn-sm btn-info'>Edit</a>
                            <a href='delete.php?id=<?php echo $row['id']; ?>' class='btn btn-sm btn-danger'>Delete</a>
                        </td>
                    </tr>
                    
                    
                             <?php
                                                               
                            endforeach;
                        }
                    ?>
                </tbody>
                
                
            </table>

<?php
include ("footer.php");
?>