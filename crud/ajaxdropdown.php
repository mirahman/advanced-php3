<?php
include "pdo.php";

$sql = "select * from drivers";
$query = $dbh->query($sql, PDO::FETCH_OBJ);

include ("header.php");
?>

<h1>Book A Taxi</h1>

<form action="create.php" method="POST" enctype='multipart/form-data'>
  <div class="form-group">
    <label for="exampleInputEmail1">Customer Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter your name" name="name">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Driver</label>
    <select  class="form-control" id="driver" name="driver">
        <option value="">
            --- select --- </option>
            <?php foreach($query as $row):
             ?>
                <option value="<?php echo $row->id?>"><?php echo $row->driverName?></option>
             <?php
            endforeach;
            ?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Taxi</label>
    <select  class="form-control" id="taxi" name="taxi">
        
    </select>
  </div>
    
  
  <button type="submit" class="btn btn-success">Submit</button>
</form>



<script>

$(function(){
    $("#driver").on('change',function(){
         console.log($(this).val());
         
         $.ajax({
           type: 'get',
            url: 'getdata.php?id='+$(this).val(),
            success: function(text) {
                console.log(text);
                $("#taxi").html(text);
            }
         });


    });
});

</script>
<?php
include ("footer.php");
?>


