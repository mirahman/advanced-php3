<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait security {
    
    public function checkUserName() {
        echo "I am from user name";
    }
    
    public function showUserLog() {
        echo "I am from user log";
    }
    
}

trait authorization {
    
    public function checkAccess() {
        echo "I am from check access";
    }
    
    public function getAccess() {
        echo "I am from get access";
    }
}

Class Kabadi {
    
    use authorization, security;
    
}

$kabadi = new Kabadi;

$kabadi->showUserLog();