<?php
class Sports  {
    
    public function myName() {
        self::getName();
        //Football::getName();
        //$this->getName();
    }
    
     
    public function getName() {
        echo " i am Sports";
    }
}

class Football extends Sports {
    
    
    public function getName() {
        echo " i am Football";
    }
}

class EPL extends Football {
    
    public function getName() {
        Sports::getName();
        echo " i am Football";
    }
}

$cric = new Football();
$cric->myName();