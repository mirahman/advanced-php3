<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class InvalidNumberException extends Exception {};
Class InvalidOperationException extends Exception {};
Class InvalidStringException extends Exception {};

try {

$a = $_GET['a'];
$b = $_GET['b'];
$opt = $_GET['opt'];

    try {
        
        if($a <10 )
            throw  new InvalidNumberException ("Please enter a big number");
    
    } catch (InvalidStringException $ex) {
            echo "i am inside nested try<br />";
            echo "<b>Inside Message:</b>".$ex->getMessage()."<br />";
    }

    echo "i am going to start operation<br />";

switch($opt)
{
    case "add":
        if(is_numeric($a)&& is_numeric($b))
        {
            echo $a+$b;
        } else {
            throw new InvalidNumberException("You can not add string. must provide number");
        }
     break;
     
     case "subtract":
        if(is_numeric($a)&& is_numeric($b))
        {
            if($a > $b)
            echo $a-$b;
            else
               throw new InvalidNumberException("A must be greater than B");
        } else {
            throw new InvalidNumberException("You can not subtract string. must provide number");
        }
     break;
     
    default:
        throw new InvalidStringException("Operation not allowed");
}


echo "i am done with all exception handling<br />";
} catch (InvalidNumberException $e) {
    echo "I am from invalid number catch<br />";
    echo "<b>Message:</b>".$e->getMessage()."<br />";
} catch (InvalidOperationException $e) {
    echo "I am from invalid operation catch<br />";
    echo "<b>Message:</b>".$e->getMessage()."<br />";
} catch (Exception $e) {
    echo "I am inside catch<br />";
    echo "<b>Message:</b>".$e->getMessage()."<br />";
}

echo "i am outside try<br />";

