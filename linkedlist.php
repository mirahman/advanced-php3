<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Node {
    
    public $name;
    public $prev;
    public $next;
   
   
}

//$nodes = [];

$head = new Node();
$head->name = "head";

$node1 = new Node();
$node1->name = "Node 1";

$head->next = $node1;
$node1->prev = $head;

$node2 = new Node();
$node2->name = "Node 2";

$node1->next = $node2;
$node2->prev = $node1;

$node3 = new Node();
$node3->name = "Node 3";

$node2->next = $node3;
$node3->prev = $node2;

function displayNextNodes($node) {
    echo $node->name."<br />";
    if($node->next)
    echo "Next is: ". displayNextNodes($node->next);
    else
        return;
}

displayNextNodes($head);

