<?php

class Human {
    
    var $name = "test";
    var $gender = "male";

    
    function __construct($uName, $uGender) {
        //echo "I am being called with ".$uName." & ".$uGender ;
        $this->name = $uName;
        $this->gender = $uGender;
    }
    
    
    function showName()
    {
        echo "I am showing name ".$this->name."<br/>";
        
        return $this;
    }
    
    function showGender()
    {
        echo "I am showing Gender ".$this->gender."<br/>";
        return $this;
        
    }
    
   
}

$me = new Human('Mizan', "M");

$me->showName()->showGender();

/*
$this->db->from()->where()->order_by()->groupBy()->limit()->get();

$this->db->from();
$this->db->where();
$this->db->order_by();
$this->db->groupBy();
$this->db->limit();
$this->db->get();

*/
