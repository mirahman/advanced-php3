<?php

function doSum(...$numbers) {
    echo array_sum($numbers)."<br />";
}

doSum(1);
doSum(1,2,3);
doSum(1,4,5,5,6,6,6,6,6,6,6,6,6,6,7);


function oneSum() {
    $args = func_get_args();
    
    echo array_sum($args);
    
    $totalArgs = func_num_args();
    if($totalArgs == 1) {
        // call one function
    } else if($totalArgs == 2) {
        // call another function
    }
    print_r($args);
}

oneSum(1,1);
oneSum(1,2,3,4,5,6);