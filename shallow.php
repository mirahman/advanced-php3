<?php
echo "<pre>";
class Sport {
    
    public $name = "";
    public $origin = "";
    public $teamSize = "";
    public $duration = "";
    public $geoCode;
    

    
    function __construct($name, $origin, $teamSize, $duration, $geocode) {
        //echo "I have been called<Br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->teamSize = $teamSize;
        $this->duration = $duration;
        $this->geoCode = $geocode;
    }
    

    
    
    function showName() {
        echo "My name is :".$this->name."<br/>";
    }
    
    function showTeamSize() {
       echo "I am showing the team size"; 
    }
}


class GeoCode {
    
    public $lon;
    public $lat;
    
    public function __construct($lon, $lat) {
       $this->lon = $lon;
       $this->lat = $lat;
    }
    
}

$geoCode = new GeoCode("123","456");

$football = new Sport('Football','England','11','90', $geoCode);

$newObj = clone $football;

echo $football->geoCode->lon."<br />";
echo $newObj->geoCode->lon."<br />";

$football->geoCode->lon = "789";

echo $football->geoCode->lon."<br />";
echo $newObj->geoCode->lon."<br />";

$football->showName();
$newObj->showName();

$football->name = "Bangladeshi Football";

$football->showName();
$newObj->showName();