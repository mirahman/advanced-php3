<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait security {
    
    public function checkUserName() {
        echo "I am from user name";
    }
    
    public function showUserLog() {
        echo "I am from user log";
    }
    
    public function showName() {
        echo "i am from security";
    }
}

trait authorization {
    
    public function checkAccess() {
        echo "I am from check access";
    }
    
    public function getAccess() {
        echo "I am from get access";
    }
    
    public function showName() {
        echo "i am from authorization<br />";
    }
}

trait sportstrait {
    use authorization, security {
        authorization::showName insteadof security;
        security::showName as securityShowName;
    }
}



Class Kabadi {
    
    use sportstrait;
    
    public function getAccess() {
        echo "i am from kabadi class<br />";
    }
    
}

$kabadi = new Kabadi;

$kabadi->showName();
$kabadi->securityShowName();