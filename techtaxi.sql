-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2016 at 01:20 PM
-- Server version: 5.6.20
-- PHP Version: 5.6.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `techtaxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
`id` int(11) NOT NULL,
  `driverName` varchar(45) DEFAULT NULL,
  `driverPicture` varchar(45) DEFAULT NULL,
  `licenseNumber` varchar(45) DEFAULT NULL,
  `dob` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `verified` tinyint(4) DEFAULT NULL,
  `feedbackScore` float DEFAULT NULL,
  `safetyCheck` float DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `driverName`, `driverPicture`, `licenseNumber`, `dob`, `gender`, `verified`, `feedbackScore`, `safetyCheck`) VALUES
(3, 'mizanur rahman', 'logo.png', '12345', '1980-12-21', 'Male', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE IF NOT EXISTS `owners` (
`id` int(11) NOT NULL,
  `ownerName` varchar(45) DEFAULT NULL,
  `gender` enum('M','F','O') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `taxis`
--

CREATE TABLE IF NOT EXISTS `taxis` (
`id` int(11) NOT NULL,
  `ownerID` int(11) DEFAULT NULL,
  `taxiNumber` varchar(45) DEFAULT NULL,
  `addedOn` date DEFAULT NULL,
  `taxiType` enum('Microbus','Toyota','Sportscar') DEFAULT NULL,
  `carModel` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `carPicture` varchar(45) DEFAULT NULL,
  `driverID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `userType` enum('Driver','Owner','Admin','Customer') DEFAULT 'Customer',
  `joinedOn` datetime DEFAULT NULL,
  `status` enum('Active','Inactive','Blocked') DEFAULT 'Inactive'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userName`, `password`, `userType`, `joinedOn`, `status`) VALUES
(2, 'mizan', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', NULL, 'Inactive'),
(3, 'mash', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'Admin', NULL, 'Inactive'),
(4, 'mustafiz', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin', NULL, 'Inactive'),
(5, 'sabbir', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin', NULL, 'Inactive'),
(8, 'sabbirx', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin', NULL, 'Inactive');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
 ADD PRIMARY KEY (`id`), ADD FULLTEXT KEY `driverName` (`driverName`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email_UNIQUE` (`email`), ADD UNIQUE KEY `phone_UNIQUE` (`phone`), ADD KEY `fk_owners_userID_idx` (`userID`), ADD KEY `ix_owners_ownerName` (`ownerName`);

--
-- Indexes for table `taxis`
--
ALTER TABLE `taxis`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_taxis_ownerID_idx` (`ownerID`), ADD KEY `fk_taxis_driverID_idx` (`driverID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `userName` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `owners`
--
ALTER TABLE `owners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxis`
--
ALTER TABLE `taxis`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `owners`
--
ALTER TABLE `owners`
ADD CONSTRAINT `fk_owners_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `taxis`
--
ALTER TABLE `taxis`
ADD CONSTRAINT `fk_taxis_driverID` FOREIGN KEY (`driverID`) REFERENCES `drivers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_taxis_ownerID` FOREIGN KEY (`ownerID`) REFERENCES `owners` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
