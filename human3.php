<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Human {
    
    var $name = "test";
    var $gender = "male";

    
    function __construct($uName, $uGender) {
        //echo "I am being called with ".$uName." & ".$uGender ;
        $this->name = $uName;
        $this->gender = $uGender;
    }
    
    function __destruct() {
        echo "SOS";
    }
    
    function showName()
    {
        echo "I am showing name ".$this->name."<br/>";
    }
    
   
}

$obj1 = new Human("Mizan","Male");
$obj2 = new Human("Mikhael","Male");

unset($obj2);

$obj1->name = "Bangladesh";
echo "I am end of script";