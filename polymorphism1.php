<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Name {    
    public function showName();    
}


Class Football implements Name {
    
    public function showName() {
        echo "I am Football";
    }
}

Class Cricket implements Name {
    
    public function showName() {
        echo "I am Cricket";
    }
}

Class Kabadi implements Name {
    
    public function showName() {
        echo "I am Kabadi";
    }
    
    public function anotherShowName() {
        
    }
}

Class Bouchi implements Name {
    
    public function showName() {
        echo "I am Bouchi";
    }
}

$goat = new Bouchi();
$cat = new Kabadi;
$lion = new Cricket;
$tiger = new Football;


$objArray = [$goat, $cat, $lion, $tiger];

function sportsName(Name $obj) {
    $obj->showName();
}

foreach($objArray as $obj)
{
    sportsName($obj);
}
