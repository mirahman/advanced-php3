<?php
echo "<pre>";
class Sport {
    
    var $name = "";
    var $origin = "";
    var $teamSize = "";
    var $duration = "";
    

    
    function __construct($name, $origin, $teamSize, $duration) {
        echo "I have been called<Br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->teamSize = $teamSize;
        $this->duration = $duration;

    }
    
    function __destruct() {
        
        
        echo "End of object life :( ";
    }
    
    
    function __get($name) {
        echo "Sorry I do not have property called '$name'";
    }
     
    
    function __set($name, $value) {
        echo "You can not add properties from outside";
    }
     
    
    function __call($name,$arguments) {
        echo "You must have called a wrong method :) ";
    }
    
    function __callStatic($name,$arguments) {
        echo "You must have called a wrong static method :) ";
    }
    
    function showName() {
        echo "My name is :".$this->name."<br/>";
    }
    
    function showTeamSize() {
       echo "I am showing the team size"; 
    }
}

$football = new Sport('Football','England','11','90');
$cricket  = new Sport('Cricket','England','11','90');
$kabadi   = new Sport('Kabadi','Bangladesh','8','30');

$cricket = null;

echo $football->coach = "Pep Gardiola";

print_r($football);
print_r($kabadi);

$epl = new Sport("English Premier League",'England','11','90');
$epl->bestTeam = "MANU";

$football->showMeScore();

Sport::showMeCards();

$serialStr = serialize($football);

echo "<br />";
echo $serialStr;
echo "<br />";

$spanishLeauge = unserialize($serialStr);

print_r($spanishLeauge);