<?php

require 'vendor/autoload.php';


$app = new Slim\App();

$host = "localhost";
$user = "root";
$pass = "";
$dbname = "techtaxi";

$dsn = "mysql:host=$host;dbname=$dbname";

$pdo =   new PDO($dsn, $user, $pass);

//function defination to convert array to xml
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("book");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

$app->post("/addDriver", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    $post = $request->getParams();
    
     $sql = "INSERT INTO drivers "
        . " SET "
        ."driverName = ?,"
        . "licenseNumber = ?";

$stmt = $pdo->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $license);


$dName = $post['name'];
$license = $post['license'];


$stmt->execute();
$response->write("added successfully");

    return $response;
});

$app->put("/updateDriver", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    $post = $request->getParsedBody();
    
    //print_r($post);
    
     $sql = "UPDATE drivers "
        . " SET "
        ."driverName = ?,"
        . "licenseNumber = ?"
             . " WHERE id = ?";

$stmt = $pdo->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $license);
$stmt->bindParam(3, $id);

$dName = $post['name'];
$license = $post['license'];
$id = $post['id'];


$stmt->execute();
$response->write("updated successfully");

    return $response;
});

$app->get("/getDriverList/{type}", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    $result = $pdo->query("select * from drivers limit 1");

    $names = [];
    foreach ($result as $row) {
        $names[] = ["id" => $row['id'],"name" => $row['driverName'],"license" => $row['licenseNumber'],"gender" => $row['gender']];
    }    
    switch($args['type']) {
        case 'csv':        
        case 'txt':
            $response->write(implode(",", $names));
        break;
    
        case 'xml':
            $xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><info></info>");
            array_to_xml($names,$xml_user_info);
            $response->write($xml_user_info->asXML());
        break;
    
        case 'json':
            $response->write(json_encode($names));
        break;
    
         default:
            $response->write(json_encode($names));
        break;
    }
    
    //$response->write(json_encode($names));
    return $response;
});

$app->get('/', function ($request, $response, $args) {
    $response->write("Welcome to home page - mizan ");
    return $response;
});

$app->get('/show/{name}', function ($request, $response, $args) {
    $newResponse = $response->withStatus(400);
    //$newResponse->write("Welcome  " . $args['name']);
    return $newResponse;
});

$app->run();
