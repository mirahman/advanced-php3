<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface DeshiSports {
    
    public function sportName();
    public function sportOrigin();
    public function teamSize();
}

interface Health extends DeshiSports {
    public function caloryRequired();
    public function burnedCalory();
}

class Kabadi implements DeshiSports, Health {
    public function sportName() {
        
    }
    public function sportOrigin() {
        
    }
    public function teamSize() {
        
    }
    
    public function caloryRequired() {
        
    }
    public function burnedCalory() {
        
    }
}

