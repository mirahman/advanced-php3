<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<pre>";
class Sport {
    
    public $name = "ABCD";
    private $origin = "England";
    protected $teamSize = "10";
    public $duration = "90";
    
    public function showName() {
        echo "My name is :".$this->name."<br/>";
    }
    
    function showTeamSize() {
       echo "I am showing the team size"; 
    }
}

class Football extends Sport {
    
    public $name = "i am child";
    
    private function showName() {
        //echo parent::showName();
        echo "test";
    }
    
    private function showTeamSize() {
       echo "I am showing the team size of child"; 
       //parent::showTeamSize();
    }
}

class EPL extends Football {
    
}

$sport = new Sport();
$sport->showTeamSize();

$football = new Football();
$football->showName();
//$cricket  = new Sport;

$football->showTeamSize();

print_r($football);
