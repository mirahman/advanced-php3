<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Football {
    
    public $country = "";
    public $startedIn = "";
    public $totalTouraments = "";
    private $totalMatches = "";
    protected $tier = "";
    

        
    
    public function __construct($country, $startedIn, $totalTouraments,$totalMatches,$tier ) {
        
        echo "I am constructing";
        
       $this->country = $country;
       $this->startedIn = $startedIn;
       $this->totalTouraments = $totalTouraments;
       $this->totalMatches = $totalMatches;
       $this->tier = $tier;
    }
    
    
    public function getCountry()
    {
        return "From Parent Class: ".$this->country;
    }
    
    public function setCountry($name) 
    {
        //DhakaFootball::getName();
        
        $this->country = $name;
    }
    
    public function getStartedIn()
    {
        return $this->startedIn;
    }
    
    public function setStartedIn($name) 
    {
        $this->startedIn = $name;
    }
    
    public function getParentConstructor() {
        return parent::__construct();
    }
    
}

Class PakiFootball extends Football { 
    
}

Class BDFootball extends Football {
        //
    public $presidentName= "";
    public $fields = "";
    
    
    public function __construct($country, $startedIn, $totalTouraments,$totalMatches,$tier ,$presidentName, $fields) {
        
        echo "I am constructing from child";
        
       parent::__construct($country, $startedIn, $totalTouraments, $totalMatches, $tier);
       
       //Sport::__construct();
       
       $this->presidentName = $presidentName;
       $this->fields = $fields;
    }
    
    
    public function getPresidentName()
    {
        return $this->presidentName;
    }
    
    public function setPresidentName($name)
    {
        $this->presidentName = $name;
    }
    
    // overriding parent method
    public function getCountry()
    {
        return "From Child Class: ".$this->country;
    }
    
}

Class DhakaFootball extends BDFootball {
    
}

$worldCup = new Football("Uruguye", "1904", "3","38","5");

print_r($worldCup);

echo $worldCup->getCountry();

$laLega = new BDFootball("Bangladesh", "1910", "3","38","5","Kazi Salahuddin","5");

// don't call directly
//echo $laLega->country;

print_r($laLega);

echo $laLega->getCountry();



// dont set directly
//$laLega->country = "Bangladesh";

$laLega->setCountry("Bangladesh");