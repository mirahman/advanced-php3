<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<pre>";

$xml =<<<STR
<students>
<student>
    <name>Test</name>
    <age>21</age>
</student>
<student>
    <name>Test one</name>
    <age>50</age>
</student>
</students>        
STR;

//echo $xml;

$obj = simplexml_load_string($xml);

//echo $obj->student[0]->name;

foreach($obj->xpath("//student[age < 25]/name") as $student)
{
    //echo($student->name)."<br />";
    echo $student."<br />";
}

print_r(xml_to_array($xml));


function xml_to_array($xml, $main_heading = '') {
    $deXml = simplexml_load_string($xml);
    $deJson = json_encode($deXml);
    //echo $deJson;
    $xml_array = json_decode($deJson, TRUE);
    if (!empty($main_heading)) {
        $returned = $xml_array[$main_heading];
        return $returned;
    } else {
        return $xml_array;
    }
}
