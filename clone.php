<?php
echo "<pre>";
class Sport {
    
    public $name = "";
    public $origin = "";
    public $teamSize = "";
    public $duration = "";
    

    
    function __construct($name, $origin, $teamSize, $duration) {
        //echo "I have been called<Br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->teamSize = $teamSize;
        $this->duration = $duration;

    }
    

    
    
    function showName() {
        echo "My name is :".$this->name."<br/>";
    }
    
    function showTeamSize() {
       echo "I am showing the team size"; 
    }
}

$football = new Sport('Football','England','11','90');
$cricket  = new Sport('Cricket','England','11','90');
$kabadi   = new Sport('Kabadi','Bangladesh','8','30');

$newObj = clone $football;

$football->showName();
$newObj->showName();

$football->name = "Bangladeshi Football";

$football->showName();
$newObj->showName();