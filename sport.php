<?php
echo "<pre>";
class Sport {
    
    var $name = "";
    var $origin = "";
    var $teamSize = "";
    var $duration = "";
    
    function showName() {
        echo "My name is :".$this->name."<br/>";
    }
    
    function showTeamSize() {
       echo "I am showing the team size"; 
    }
}

$football = new Sport();
$cricket  = new Sport;

print_r($football);

echo $football->teamSize;
echo $cricket->teamSize;

$football->name = "Football";
$football->origin = "England";
$football->teamSize = "11";

$cricket->name = "Cricket";
$cricket->origin = "England";
$cricket->teamSize = "6";

print_r($football);
print_r($cricket);

echo $football->showName();
echo $cricket->showName();

//echo showTeamSize();
